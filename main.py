'''
PUSH SWAP
'''
from moves import Stack, sorted
from method_3 import sort_3
from method_insertion import sort_100

s = Stack()

def main():
    s.a = [3, 1, 2, 5, 234234, 234, 454, 23, 355,17, 437, 28,43,87,]
    s.a = [5, 2, 1, 3]

    print(s.a, sorted(s.a))
    if len(s.a) is 3:
        sort_3(s)
    else:
        sort_100(s)

    print(s.a, sorted(s.a))
    if sorted(s.a):
        print("Sorted Stack A in", s.moves, "moves!")
    pass


if __name__ == "__main__":
    main()
