def sorted(stack: object) -> bool:
    tmp = stack.copy()
    tmp.sort()
    tmp.reverse()
    # print(tmp, stack)
    return True if stack == tmp else False

class Stack():
    a = []
    b = []

    moves = 0

    def sa(self):
        # Swap top two numbers in Stack A
        if len(self.a) < 2:
            return
        top = self.a.pop()
        second = self.a.pop()
        self.a.append(top)
        self.a.append(second)

        self.moves += 1
        pass

    def sb(self):
        # Swap top two numbers in Stack B
        if len(self.b) < 2:
            return
        top = self.b.pop()
        second = self.b.pop()
        self.b.append(top)
        self.b.append(second)

        self.moves += 1
        pass

    def ss(self):
        # Runs sa and sb at the same time
        self.sa()
        self.sb()

        self.moves -= 1
        pass

    def ra(self):
        # Top number goes to the bottom of Stack A
        if len(self.a) < 2:
            return
        top = self.a.pop()
        self.a.reverse()
        self.a.append(top)
        self.a.reverse()

        self.moves += 1
        pass

    def rb(self):
        # Top number goes to the bottom of Stack B
        if len(self.b) < 2:
            return
        top = self.b.pop()
        self.b.reverse()
        self.b.append(top)
        self.b.reverse()

        self.moves += 1
        pass

    def rr(self):
        # Runs ra and rb at the same time
        self.ra()
        self.rb()

        self.moves -= 1
        pass

    def rra(self):
        # Bottom number goes to the top of Stack A
        if len(self.a) < 2:
            return
        self.a.reverse()
        bot = self.a.pop()
        self.a.reverse()
        self.a.append(bot)

        self.moves += 1
        pass

    def rrb(self):
        # Bottom number goes to the top of Stack B
        if len(self.b) < 2:
            return
        self.b.reverse()
        bot = self.b.pop()
        self.b.reverse()
        self.b.append(bot)

        self.moves += 1
        pass

    def rrr(self):
        # Runs rra and rrb at the same time
        self.rra()
        self.rrb()

        self.moves -= 1
        pass

    def pa(self):
        # Send top of B to the top of A
        if len(self.b) > 0:
            self.a.append(self.b.pop())
            self.moves += 1
        pass

    def pb(self):
        # Send top of A to the top of B
        if len(self.a) > 0:
            self.b.append(self.a.pop())
            self.moves += 1
        pass