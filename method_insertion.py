from moves import *
from math import inf

def sort_100(s: Stack):

    while len(s.a) > 0:
        smallest = (inf, -1)
        for i, n in enumerate(s.a):
            if n < smallest[0]:
                smallest = (n, len(s.a) - i)

        for n in range(smallest[1] - 1):
            s.ra()
        s.pb()

    for n in range(len(s.b)):
        s.pa()

    pass

