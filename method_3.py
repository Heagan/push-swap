from moves import *


def sort_3(s: Stack):
    if len(s.a) < 3:
        return
        
    fir = 3 if s.a[2] > s.a[1] and s.a[2] > s.a[0] else 1 if s.a[2] < s.a[1] and s.a[2] < s.a[0] else 2
    sec = 3 if s.a[1] > s.a[2] and s.a[1] > s.a[0] else 1 if s.a[1] < s.a[2] and s.a[1] < s.a[0] else 2
    trd = 3 if s.a[0] > s.a[1] and s.a[0] > s.a[2] else 1 if s.a[0] < s.a[1] and s.a[0] < s.a[2] else 2

    fir = s.a[2]
    sec = s.a[1]
    trd = s.a[0]

    # print(s.a, (fir, sec, trd))

    if fir > sec and fir < trd and sec < trd and fir < trd:
        s.sa()
        print("1")
    if fir > sec and fir > trd and sec > trd:
        s.sa()
        s.rra()
        print("2")
    if fir > sec and fir > trd and sec < trd and fir > trd:
        s.ra()
        print("3")
    # print(fir < sec, fir < trd, sec > trd)
    if fir < sec and fir < trd and sec > trd:
        s.sa()
        s.ra()
        print("4")
    if fir < sec and fir > trd and sec > trd:
        s.rra()
        print("5")
    # s.a.reverse()
    pass